<?php

function get_all_data($conn)
{
	$query = 
	'select 
	p.id,p.nome,p.sku,p.preco,p.quantidade
    ,cp.id_categories
    ,group_concat(c.nome) categorias
	from products p
	left join categories_per_product cp on cp.id_products=p.id
	left join categories c on cp.id_categories=c.id
	where id_categories is not null
	group by p.id';
	try	
	{
		$result = mysqli_query($conn,$query);
	} 	
	catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";	
	}
	
	if(mysqli_num_rows($result) <> 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{
	    echo '
		<tr class="data-row">
			<td class="data-grid-td">
			<span class="data-grid-cell-content">'.$row['nome'].'</span>
			</td>
		
			<td class="data-grid-td">
			<span class="data-grid-cell-content">'.$row['sku'].'</span>
			</td>
	
			<td class="data-grid-td">
			<span class="data-grid-cell-content">R$ '.$row['preco'].'</span>
			</td>
	
			<td class="data-grid-td">
			<span class="data-grid-cell-content">'.$row['quantidade'].'</span>
			</td>
	
			<td class="data-grid-td">
			<span class="data-grid-cell-content">';
				
				// Consome as categorias do produto separados por comma, retornadas pelo group_concat()
				$variableAry = explode(",",$row['categorias']);
				$len = count($variableAry);
				$i = 0;
				# Category 1 <Br />Category 2
				foreach($variableAry as $var)
				{
					echo $var;
					echo ($i <> $len - 1) ? ' <Br />' : '';
					$i++;
				}
			echo
			'</span>
			</td>
		
			<td class="data-grid-td">
			<div class="w3-bar">
				<button class="w3-bar-item w3-button w3-blue" value="edit,'.$row['id'].','.$row['nome'].'">Edit</button>
				<button class="w3-bar-item w3-button w3-red" value="delete,'.$row['id'].','.$row['nome'].'">Delete</button>
			</div> 
			</td>
		</tr>';
		}
	
		mysqli_free_result($result);
	}
	else
	{
		echo "<script>alert('Sem produtos para listar.')</script>";
	}
	exit;
}
?>