$(document).ready(function(){ 
    $('.w3-button').click(function()
	{
        var clickBtnValue = $(this).val();
		var args = 	clickBtnValue.split(",");
		var mode = args[0];
		var cat_nome = args[1];
		var cat_id = args[2];
		// Edit
		if(args[0] == "edit")
		{
			window.location.replace("../addCategory.php?id="+cat_id);	
		}
		// Delete
		else if (args[0] == "delete")
		{
			var ajaxurl = '../crud_funcs/delete_category.php';	
			$.post(ajaxurl, {'action': cat_nome+','+cat_id}, function (response) {
				alert(response);
				location.reload();
			});
		}
        });
});