<?php
	require 'db_connection.php';
	
	if (isset($_POST['action'])) 
	{
		$conn = $_SESSION['conn'];
		$variableAry = explode(",",$_POST['action']);
		$prod_id = mysqli_real_escape_string($conn,$variableAry[0]);
		$prod_nome = mysqli_real_escape_string($conn,$variableAry[1]);
		
		$query = 'delete from products where id='.$prod_id.';';
		date_default_timezone_set('America/Sao_paulo');
		$time = date('d/m/Y \à\s H:i:s',time());
		$log = "insert into logs (id,descricao,timestamp) values 
			(0,
			'Produto ".$prod_nome." excluido em ".$time."',
			now())";
        try	
		{
			mysqli_query($conn,$query);
			echo (mysqli_affected_rows($conn) > 0) ? 'Produto excluído com sucesso.' : mysqli_error($conn);
			// Escreve no log
			mysqli_query($conn,$log);
		} 	
		catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";	
		}
		exit;
	}
?>