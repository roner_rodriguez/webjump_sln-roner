<?php
	session_start();
	require_once 'crud_funcs\db_connection.php';
	$conn = $_SESSION['conn'];

	if(isset($_GET['id']) && is_numeric($_GET['id']))
	{
		$query = "select nome,codigo from categories where id=".$_GET['id'];
		
		try	
		{
			$result = mysqli_query($conn,$query);
			if(mysqli_num_rows($result) > 0)
			{
				$edit_mode = 1;
				$row = mysqli_fetch_assoc($result);
				$_SESSION['old_name'] = $cname = $row['nome'];
				$_SESSION['old_code'] = $ccode = $row['codigo'];
			}
			mysqli_free_result($result);
		}
		catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";	
		}
	}
	else
	{
		$edit_mode = 0;
	}
?>
<!doctype html>
<html ⚡>
<head>
  <title>Webjump | Backend Test | Add Category</title>
  <meta charset="utf-8">

<link  rel="stylesheet" type="text/css"  media="all" href="css/style.css" />
<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,800" rel="stylesheet">
<meta name="viewport" content="width=device-width,minimum-scale=1">
<style amp-boilerplate>body{-webkit-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-moz-animation:-amp-start 8s steps(1,end) 0s 1 normal both;-ms-animation:-amp-start 8s steps(1,end) 0s 1 normal both;animation:-amp-start 8s steps(1,end) 0s 1 normal both}@-webkit-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-moz-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-ms-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@-o-keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}@keyframes -amp-start{from{visibility:hidden}to{visibility:visible}}</style><noscript><style amp-boilerplate>body{-webkit-animation:none;-moz-animation:none;-ms-animation:none;animation:none}</style></noscript>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <script src="js/jquery-3.5.1.min.js"></script>
</head>
  <!-- Header -->
<amp-sidebar id="sidebar" class="sample-sidebar" layout="nodisplay" side="left">
  <div class="close-menu">
    <a on="tap:sidebar.toggle">
      <img src="images/bt-close.png" alt="Close Menu" width="24" height="24" />
    </a>
  </div>
  <a href="dashboard.html"><img src="images/menu-go-jumpers.png" alt="Welcome" width="200" height="43" /></a>
  <div>
    <ul>
      <?php require 'menu.php'; ?>
    </ul>
  </div>
</amp-sidebar>
<header>
  <div class="go-menu">
    <a on="tap:sidebar.toggle">☰</a>
    <a href="dashboard.html" class="link-logo"><img src="images/go-logo.png" alt="Welcome" width="69" height="430" /></a>
  </div>
  <div class="right-box">
    <span class="go-title">Administration Panel</span>
  </div>    
</header>  
<!-- Header -->
  <!-- Main Content -->
  <main class="content">
    <h1 class="title new-item"><?php echo ($edit_mode == 1) ? 'Edit Category' : 'New Category';?></h1>
    
    <form action='crud_funcs\create_category.php' method="post">
      
	  <input id="edit_mode" name="edit_mode" type="hidden" value="<?php echo $edit_mode; ?>" />
	  
	  <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input required type="text" id="category-name" name="category-name" class="input-text" 
			value="<?php echo isset($cname) ? $cname : '';?>" />
        
      </div>
      <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input required type="text" id="category-code" name="category-code" class="input-text" 
			value="<?php echo isset($ccode) ? $ccode : '';?>" />
        
      </div>
      <div class="actions-form">
        <a href="categories.php" class="action back">Back</a>
        <input class="btn-submit btn-action"  type="submit" 
			value="<?php echo ($edit_mode == 1) ? 'Edit' : 'Save';?>" />
      </div>
    </form>
  </main>
  <!-- Main Content -->

  <!-- Footer -->
<footer>
	<div class="footer-image">
	  <img src="images/go-jumpers.png" width="119" height="26" alt="Go Jumpers" />
	</div>
	<div class="email-content">
	  <span>go@jumpers.com.br</span>
	</div>
</footer>
 <!-- Footer -->

 </body>
</html>
<?php mysqli_close($_SESSION['conn']); ?>
