<?php
function get_all_data($conn)
{
	$query = 
		'SELECT id,nome,codigo FROM categories;';
	
	try	
	{
		$result = mysqli_query($conn,$query);
	} 	
	catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";	
	}
	
	if(mysqli_num_rows($result) > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{
		echo '
		<tr class="data-row">
			<td class="data-grid-td">
			<span class="data-grid-cell-content">'.$row['nome'].'</span>
			</td>
		
			<td class="data-grid-td">
			<span class="data-grid-cell-content">'.$row['codigo'].'</span>
			</td>
		
			<td class="data-grid-td">
			<div class="w3-bar">
				<button class="w3-bar-item w3-button w3-blue" value="edit,'.$row['nome'].','.$row['id'].'">Edit</button>
				<button class="w3-bar-item w3-button w3-red" value="delete,'.$row['nome'].','.$row['id'].'">Delete</button>
			</div> 
			</td>
		</tr>	
		';
		}
		mysqli_free_result($result);
	}
	else
	{
		echo "<script>alert('Sem produtos para listar.')</script>";
	}
}
?>