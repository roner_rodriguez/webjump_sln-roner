<?php
function get_all_data($conn,$prod_id,$mode)
{
	// Todas categorias
	if($mode == 0)
	{
		$query = "select group_concat(nome) cnomes from categories"; 
	}
	// Categorias registradas para o produto + nao registradas
	else if($mode == 1)
	{
		$query = 
		"SELECT 
		group_concat(c.nome) cnomes,
		(select group_concat(c2.nome) from categories c2 where c2.id not in 
		(select id_categories from categories_per_product cp2 where cp2.id_products=".$prod_id.")) 
		cnomes2
		FROM categories c
		left join categories_per_product cp on cp.id_products=".$prod_id."   
		where c.id=cp.id_categories;";
	}
		
	try	
	{ 
		$result = mysqli_query($conn,$query);
	} 	
	catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";	
	} 	
 
	if(mysqli_num_rows($result) > 0) 
	{		
		if($mode == 0)
		{
			$row = mysqli_fetch_assoc($result);
			$variableAry = explode(",",$row['cnomes']);
			
			echo "<option disabled> • Available </option>";
			foreach($variableAry as $var)
			{
				echo "<option>".$var."</option>";
			}
		}
		else if($mode == 1)
		{
			$row = mysqli_fetch_assoc($result);
			$variableAry = explode(",",$row['cnomes']);
			$variableAry2 = explode(",",$row['cnomes2']);
			
			echo "<option disabled> • Available </option>";
			foreach($variableAry2 as $var2)
			{
				echo "<option>".$var2."</option>";
			}
			echo "<option disabled> • Registered </option>";
			foreach($variableAry as $var)
			{
				echo "<option>".$var."</option>";
			}
		}
		
	mysqli_free_result($result);
	}
}
?>