<?php
function get_all_data($conn)
{
	$query = 'SELECT descricao FROM webjump_store.logs order by timestamp asc';
	
	try	
	{
		$result = mysqli_query($conn,$query);
	} 	
	catch (Exception $e) {
		echo 'Caught exception: ',  $e->getMessage(), "\n";	
	}
	
	if(mysqli_num_rows($result) > 0)
	{
		while($row = mysqli_fetch_assoc($result))
		{
		echo '
		<tr class="data-row">
			<th class="data-grid-th">
				<span class="data-grid-cell-content">'.$row['descricao'].'</span>
			</th>
		</tr>
		';
		}
		mysqli_free_result($result);
	}
	else
	{
		echo "<script>alert('Sem logs por enquanto.')</script>";
	}
}
?>