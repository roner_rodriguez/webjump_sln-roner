$(document).ready(function(){ 
    $('.w3-button').click(function()
	{
        var clickBtnValue = $(this).val();
		var args = 	clickBtnValue.split(",");
		var mode = args[0];
		var prod_id = args[1];
		var prod_nome = args[2];
		// Edit
		if(args[0] == "edit")
		{
			window.location.replace("../addProduct.php?id="+prod_id);	
		}
		// Delete
		else if (args[0] == "delete")
		{
			var ajaxurl = '../crud_funcs/delete_product.php';	
			$.post(ajaxurl, {'action': prod_id+','+prod_nome}, function (response) {
				alert(response);
				location.reload();
			});
		}
        });
});