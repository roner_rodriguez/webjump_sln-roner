<?php
	session_start();
	require 'db_connection.php';

    if (isset($_POST['category-code']) && isset($_POST['edit_mode']) && isset($_POST['category-name'])) 
	{
		$conn = $_SESSION['conn'];
		$time = date('d/m/Y \à\s H:i:s',time());
		$edit_mode = mysqli_real_escape_string($conn,$_POST['edit_mode']);
		$cname = mysqli_real_escape_string($conn,$_POST['category-name']);
		$ccode = mysqli_real_escape_string($conn,$_POST['category-code']);
		
		// Create 
		if($edit_mode == 0)
		{
			// Verifica se a categoria ja existe
			try	
			{
				$query = "SELECT * FROM categories where codigo="."'".$ccode."'".";";
				$result = mysqli_query($conn,$query);
				if(mysqli_num_rows($result) > 0)
				{
					echo 
					"<script>
						alert('Já existe uma categoria com esse código.');
						window.history.back();
					</script>";					
					mysqli_free_result($result);
				}
				// Insere a categoria
				else
				{
					$log = "insert into logs (id,descricao,timestamp) values 
							(0,
							'Nova categoria adicionada: ".$cname." cód. ".$ccode." em ".$time."',
							now())";
					$query = "insert into categories (id,nome,codigo) values (0,"."'".$cname."'".","."'".$ccode."'".");";
					
					try{
						mysqli_query($conn,$query);
						
						if(mysqli_affected_rows($conn) > 0)
						{
							mysqli_query($conn,$log);
							echo 
							"<script>
								alert('Nova categoria adicionada com sucesso.');
								window.history.back();
							</script>";
						}
						else
						{ 
							"<script>
								alert(".mysqli_error($conn).");
								window.history.back();
							</script>";
						}
					}
					catch (Exception $e) {
						echo 'Caught exception: ',  $e->getMessage(), "\n";	
					}
				}
			} 	
			catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";	
			}	
		}
		// Update
		else if($edit_mode == 1)
		{	
			$log = "insert into logs (id,descricao,timestamp) values 
							(0,
							'Categoria ".$_SESSION['old_name']." atualizada em ".$time.": ".
								$_SESSION['old_name']." = ".$cname.",". 
								$_SESSION['old_code']." = ".$ccode."
								',
							now())";
			$query = "update categories set nome="."'".$cname."'".",codigo="."'".$ccode."'"." where codigo="."'".$_SESSION['old_code']."'";
	
			try {
				mysqli_query($conn,$query);
				
				if(mysqli_affected_rows($conn) > 0)
				{
					mysqli_query($conn,$log);
					echo 
					"<script>
						alert('Categoria atualizada com sucesso.');
						window.history.back();
					</script>";
				}
				else
				{ 
					echo
					"<script>
						alert('Algum erro ocorreu >> '".mysqli_error($conn).");
						window.history.back();
					</script>";
				}
			} 	
			catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";	
			}
			
		}
    }
	else
	{
		echo "<script> alert('Todos os campos devem ser devidamente preenchidos.'); </script>"; 
		echo "<script> window.history.back(); </script>"; 
	}
	
	EXIT;
?>