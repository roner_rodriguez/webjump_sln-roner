# Você quer ser um desenvolvedor Backend na Web Jump?
Muito.

# Sobre a solução
A solução proposta consiste em um CRUD PHP executado sobre as seguintes tecnologias:

- Apache/2.4.43 x64
- PHP/7.4.6 x64 TS
- MySQL Server/8.019 x64

# Estrutura de diretórios
- 📂 webjump-app/
- L	📂 crud_files/ : Contem a funcionalidade necessária para o CRUD 
- L	📂 images/
- L	📂 css/ 
- L	📂 js/ 
- L	📂 images/
- 📂 dev/ : Arquivos contendo informações acerca do dev.

# Como executar
- Faça deploy do diretório webjump_app/ para um webserver php-ready (Apache, Nginx, ...) 
- Enjoy.