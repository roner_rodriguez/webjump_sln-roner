<?php
	require 'db_connection.php';
	
	if (isset($_POST['action'])) 
	{
		$conn = $_SESSION['conn'];
		$variableAry = explode(",",$_POST['action']);
		$cat_nome = mysqli_real_escape_string($conn,$variableAry[0]);
		$cat_id = mysqli_real_escape_string($conn,$variableAry[1]);
		
		$query = 'delete from categories where id='.$cat_id.';';
		date_default_timezone_set('America/Sao_paulo');
		$time = date('d/m/Y \à\s H:i:s',time());
		$log = "insert into logs (id,descricao,timestamp) values 
			(0,
			'Categoria ".$cat_nome.", cód. ".$cat_id." excluída em ".$time."',
			now())";
        try	
		{
			mysqli_query($conn,$query);
			echo (mysqli_affected_rows($conn) > 0) ? 'Categoria excluída com sucesso.' : mysqli_error($conn);
			// Escreve no log
			mysqli_query($conn,$log);
		} 	
		catch (Exception $e) {
			echo 'Caught exception: ',  $e->getMessage(), "\n";	
		}
		exit;
	}
?>