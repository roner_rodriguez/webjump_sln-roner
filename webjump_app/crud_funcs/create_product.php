<?php
	session_start();
	require_once 'db_connection.php';

    if(isset($_POST["categories"]) && isset($_POST['edit_mode'])
		&& isset($_POST["sku"]) && isset($_POST["nome"]) && isset($_POST["preco"]) && isset($_POST["descricao"]))
	{		
		$conn = $_SESSION['conn'];
		$time = date('d/m/Y \à\s H:i:s',time());
		$edit_mode = mysqli_real_escape_string($conn,$_POST['edit_mode']);
		$sku = mysqli_real_escape_string($conn,$_POST['sku']);
		$nome = mysqli_real_escape_string($conn,$_POST['nome']);
		$preco = mysqli_real_escape_string($conn,$_POST['preco']);
		$qtd = mysqli_real_escape_string($conn,$_POST['qtd']);
		$descricao = mysqli_real_escape_string($conn,$_POST['descricao']);
				
		// Create 
		if($edit_mode == 0)
		{
			// categorias ja registradas
			$categoriesAry = $_POST["categories"];
			$len = count($categoriesAry);
			$list_categories = '';
			for ($i = 0; $i < count($categoriesAry); $i++) {
				$list_categories .= mysqli_real_escape_string($conn,$categoriesAry[$i]);
				if($i < $len-1) $list_categories .= ',';
			}
			
			// Verifica se produto ja existe
			try	
			{
				$query = "SELECT * FROM products where sku="."'".$sku."'";
				$result = mysqli_query($conn,$query);
				if(mysqli_num_rows($result) > 0)
				{
					echo 
					"<script>
						alert('Já existe um produto com esse SKU.');
						window.history.back();
					</script>";					
					mysqli_free_result($result);
				} 
				// Insere o produto
				else  
				{  
					$log = "insert into logs (id,descricao,timestamp) values 
							(0,
							'Novo produto adicionado: ".$nome." SKU ".$sku." em ".$time."',
							now())";
					$query = "insert into products (id,nome,sku,descricao,quantidade,preco) values 
						(0,
						"."'".$nome."'".",
						"."'".$sku."'".",
						"."'".$descricao."'".",
						"."'".$qtd."'".",
						"."'".$preco."'".");";
					// Insere as categorias do produto 
					$query2 = 
						// 'select LAST_INSERT_ID() into @id;'.
						"select max(id) from products into @id;".
						"insert into categories_per_product (id_products,id_categories) values ";
					$variableAry = explode(",",$list_categories);
					$len = count($variableAry);
					$i = 0;
					$query3 = "";
					foreach($variableAry as $var)
					{
						$query3 .= "(@id,".
							"(select id from categories where nome="."'".$var."'"."))".(($i < $len-1) ? ',' : ';');
						$i++; 
					}
					$query4 = $query2.$query3;
										
					try{
						mysqli_query($conn,$query);
						
						if(mysqli_affected_rows($conn) > 0)
						{
							// Escreve no log
							mysqli_query($conn,$log);
							// Insere as categorias correspondentes
							mysqli_multi_query($conn,$query4);
							if(mysqli_affected_rows($conn) == 0) { mysqli_error($conn); exit; }
							echo 
							"<script>
								alert('Novo produto adicionado com sucesso.');
								window.history.back();
							</script>";
						}
						else
						{ 
							"<script>
								alert('Algum erro ocorreu: '".mysqli_error($conn).");
								window.history.back();
							</script>";
						}
					}
					catch (Exception $e) {
						echo 'Caught exception: ',  $e->getMessage(), "\n";	
					}
				}
			} 	
			catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";	
			}	
		}
		// Update
		// echo "<script> alert("."'".$query4."'"."); </script>"; 
		else if($edit_mode == 1)
		{	
			$log = "insert into logs (id,descricao,timestamp) values 
							(0,
							'Produto atualizado em ".$time."',
							now())";
			$query = "update products set nome="."'".$cname."'".",codigo="."'".$ccode."'"." where codigo="."'".$_SESSION['old_code']."'";
		
			try {
				mysqli_query($conn,$query);
				
				if(mysqli_affected_rows($conn) > 0)
				{
					mysqli_query($conn,$log);
					echo 
					"<script>
						alert('Produto atualizado com sucesso.');
						window.history.back();
					</script>";
				}
				else
				{ 
					echo
					"<script>
						alert('Algum erro ocorreu >> '".mysqli_error($conn).");
						window.history.back();
					</script>";
				}
			} 	
			catch (Exception $e) {
				echo 'Caught exception: ',  $e->getMessage(), "\n";	
			}
			
		}
    }
	else
	{
		echo "<script> alert('Todos os campos (exceto descrição e foto) devem ser devidamente preenchidos.'); </script>"; 
		echo "<script> window.history.back(); </script>"; 
	}
	
	mysqli_close($_SESSION['conn']);
	EXIT;
?>
